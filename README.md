CrazyPythonSms
==============
My wife had an Android Phone with many many SMS.
She moved to IOS and didn't want to lose all the SMS she received.

I looked for a free application to migrate all the SMS from Android to IOS but could not find **any**.
Finally I installed **SMS Backup and Restore**. It produced a big XML file with all the received SMS.

This **little** Python program processes the big export file and creates several small text files.
Each contact name will result in a dedicated text file.

I uploaded all these files to my wife's Google Drive space and she can now consult them when she needs to remember a past exchange with a contact.

When the files are consulted on a computer, the emoticons are perfectly reproduced. 
However on IOS the google drive content preview is OK but when I open the file, the UF8 is not well handled (missing accentuated letters and emoticons)

Usage: `sms2gdrive.py xml_file_name`

Requires: Python3

Tested on: Mac Os Catalina (Released 13/03/2020)