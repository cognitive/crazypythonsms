#!/usr/local/bin/python3
import sys
import re
import os
import xml.etree.ElementTree as ET
from datetime import datetime

if (len(sys.argv) < 2 or len(sys.argv) > 2):
    print("Usage sms2gdrive.py file_name")
    sys.exit()

try:
    file_name = sys.argv[1]
    tree = ET.parse(file_name)
except IOError:
    print("There was an error reading:", file_name)
    sys.exit()

root = tree.getroot()
path_prefix = "folders"
os.mkdir(path_prefix)

for sms in root.findall("./sms"):
    contact_name = sms.attrib['contact_name']
    if contact_name.find('/') > -1:
        contact_name = contact_name.replace("/"," ")
    date_sent = sms.attrib['date_sent']
    if date_sent == '0':
        continue
    # Remove millis (3 last chars)
    short_date_sent = date_sent[:-3]
    body = sms.attrib['body']
    
    mydatetime = datetime.fromtimestamp( int(short_date_sent) )
    datestr = f"{mydatetime.year:04}-{mydatetime.month:02}-{mydatetime.day:02}_{mydatetime.hour:02}:{mydatetime.minute:02}:{mydatetime.second:02}"

    file = open(path_prefix + "/" + contact_name + ".txt", "a+") 
    file.write(datestr + ": " + body + "\n")
    file.close 
